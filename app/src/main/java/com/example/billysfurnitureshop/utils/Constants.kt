package com.example.billysfurnitureshop.utils

object Constants {
    const val BASE_URL = "http://myjson.dit.upm.es/"

    const val TYPE_CART_LIST = "cart_list"
    const val TYPE_HOME_LIST = "home_list"

    const val TABLE_CART = "productCartTable"

    const val TAG_ROOM_EXCEPTION = "Room Exception"
    const val TAG_UNTREATED_FAILURE = "Untreated Failure"
}