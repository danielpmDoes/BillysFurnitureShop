package com.example.billysfurnitureshop.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.billysfurnitureshop.core.BaseViewModel
import com.example.billysfurnitureshop.data.models.Product
import com.example.billysfurnitureshop.uc.EmptyCartUC
import com.example.billysfurnitureshop.uc.GetProductsUC
import com.example.billysfurnitureshop.uc.RemoveProductFromCartUC
import com.example.billysfurnitureshop.utils.Constants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CartFragmentViewModel @Inject constructor(
    private val getProductsUC: GetProductsUC,
    private val removeProductFromCartUC: RemoveProductFromCartUC,
    private val emptyCartUC: EmptyCartUC
) : BaseViewModel(){

    private val _cartProductList = MutableLiveData<List<Product>>()
    val cartProductList : LiveData<List<Product>>
        get() = _cartProductList

    private val _productRemoved = MutableSharedFlow<Product>()
    val productRemoved : SharedFlow<Product>
        get() = _productRemoved

    fun getCartProductList(){
        getProductsUC.execute(
            { it.either(
                ::handleFailure,
                ::handleProductList
            ) },
            GetProductsUC.Params(Constants.TYPE_CART_LIST),
            viewModelScope
        )
    }

    private fun handleProductList(productList: List<Product>) {
        _cartProductList.postValue(productList)
    }

    fun removeProductFromCart(productToDelete: Product) {
        removeProductFromCartUC.execute(
            { it.either(::handleFailure, ::handleProductRemoved) },
            RemoveProductFromCartUC.Params(productToDelete),
            viewModelScope
        )
    }

    private fun handleProductRemoved(product: Product) {
        viewModelScope.launch {
            _productRemoved.emit(product)
        }
    }

    fun emptyCart() {
        emptyCartUC.execute(
            { it.either(::handleFailure) { _cartProductList.postValue(listOf()) } },
            Unit,
            viewModelScope
        )
    }

}