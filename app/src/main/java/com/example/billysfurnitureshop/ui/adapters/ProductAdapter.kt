package com.example.billysfurnitureshop.ui.adapters

import android.content.Context
import android.system.Os.remove
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.get
import androidx.recyclerview.widget.RecyclerView
import com.example.billysfurnitureshop.R
import com.example.billysfurnitureshop.core.capitalize
import com.example.billysfurnitureshop.core.noDecimalIfZeroString
import com.example.billysfurnitureshop.data.models.Product
import com.example.billysfurnitureshop.data.models.TYPE_CHAIR
import com.example.billysfurnitureshop.data.models.TYPE_COUCH
import com.example.billysfurnitureshop.databinding.ProductItemBinding
import com.example.billysfurnitureshop.ui.HomeFragment

class ProductAdapter(
    private val context: Context,
    private val fragmentTag : String,
    private var productList : MutableList<Product>,
    private val onAddToCart : ((Product) -> Unit)? = null,
    private val onRemoveFromCart: ((Product) -> Unit)? = null
) : RecyclerView.Adapter<ProductAdapter.ProductHolder>() {

    inner class ProductHolder(
        val itemBinding: ProductItemBinding
    ): RecyclerView.ViewHolder(itemBinding.root)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductHolder {
        val itemBinding = ProductItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ProductHolder, position: Int) {
        holder.itemBinding.apply {
            val productItem = productList[position]

            if(!bindImage(productImage, productItem.imgUrl))
                productImage.setImageResource(R.drawable.ic_round_warning_50)

            productName.text = productItem.name.capitalize()
            price.text = String.format(
                context.getString(R.string.item_price) ,
                productItem.price.value.noDecimalIfZeroString(),
                productItem.price.currency
            )
            type.text = productItem.type.capitalize()
            color.text = String.format(context.getString(R.string.item_color), productItem.info.color)

            when(productItem.type) {
                TYPE_CHAIR -> {
                    info.text = productItem.info.material?.capitalize()
                }
                TYPE_COUCH -> {
                    info.text = productItem.info.numberOfSeats?.let {
                        String.format(context.getString(R.string.item_seats_number), it.toString())
                    }
                }
            }

            //Button listeners and visibility
            if (fragmentTag == HomeFragment.ARG_TAG){
                unitsTextView.visibility = View.GONE
                removeFromCartButton.visibility = View.GONE
                addToCartButton.visibility = View.VISIBLE
            }

            removeFromCartButton.setOnClickListener {
                onRemoveFromCart?.let { onDelete -> onDelete(productItem) }
            }

            addToCartButton.setOnClickListener {
                onAddToCart?.let { onAdd -> onAdd(productItem) }
            }
        }
    }

    fun updateList(newList : List<Product>) {
        if (productList != newList) {
            productList = newList.toMutableList()
            notifyDataSetChanged()
        }
    }

    fun removedFromCart(product: Product) {
        val index = productList.indexOf(product)
        productList.removeAt(index)
        notifyItemRemoved(index)
    }

    override fun getItemCount() = productList.size
}