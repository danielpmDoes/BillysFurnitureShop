package com.example.billysfurnitureshop.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.example.billysfurnitureshop.R
import com.example.billysfurnitureshop.core.Failure
import com.example.billysfurnitureshop.core.formatCapitalized
import com.example.billysfurnitureshop.core.noDecimalIfZeroString
import com.example.billysfurnitureshop.data.models.Product
import com.example.billysfurnitureshop.databinding.FragmentCartBinding
import com.example.billysfurnitureshop.ui.adapters.ProductAdapter
import com.example.billysfurnitureshop.utils.Constants.TAG_UNTREATED_FAILURE
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class CartFragment : Fragment() {

    companion object {
        const val ARG_TAG = "cartFragment"
    }

    private val mViewModel : CartFragmentViewModel by viewModels()

    private var _binding: FragmentCartBinding? = null
    private val binding get() = _binding!!

    private lateinit var productAdapter : ProductAdapter
    private lateinit var currentProductList : MutableList<Product>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        context?.let { productAdapter =  ProductAdapter(it, ARG_TAG, mutableListOf(), onRemoveFromCart = ::onRemoveFromCart) }

        _binding = FragmentCartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        context?.let {
            binding.cartRecyclerView.layoutManager = GridLayoutManager(it, 1)
            binding.cartRecyclerView.adapter = productAdapter
        }

        initiateUI()
        initiateObservers()

        super.onViewCreated(view, savedInstanceState)
    }

    private fun initiateUI() {
        binding.fragmentLoader.visibility = View.VISIBLE
        mViewModel.getCartProductList()
        setTotalPriceTextView(0.0)
        binding.emptyCartButton.setOnClickListener {
            mViewModel.emptyCart()
        }
        binding.cartRecyclerView.adapter = productAdapter
    }

    private fun initiateObservers() {
        mViewModel.failure.observe(viewLifecycleOwner, ::treatFailure)

        mViewModel.cartProductList.observe(viewLifecycleOwner) { list ->
            binding.fragmentLoader.visibility = View.GONE

            if (list.isNotEmpty()){
                currentProductList = list.toMutableList()
                binding.centerCardView.visibility = View.GONE
            } else {
                binding.failureCartTextView.visibility = View.GONE
                binding.centerCardView.visibility = View.VISIBLE
                binding.emptyCartTextView.visibility = View.VISIBLE
            }

            productAdapter.updateList(list)
            setTotalPriceTextView(calculateTotalPrice(list))
        }

        lifecycleScope.launchWhenStarted {
            mViewModel.productRemoved.collectLatest {
                currentProductList.remove(it)
                setTotalPriceTextView(calculateTotalPrice(currentProductList))
                productAdapter.removedFromCart(it)

                if(currentProductList.isEmpty()) {
                    binding.centerCardView.visibility = View.VISIBLE
                    binding.emptyCartTextView.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun onRemoveFromCart(product: Product) {
        mViewModel.removeProductFromCart(product)
    }

    private fun calculateTotalPrice(cartList: List<Product>) : Double {
        var total = 0.0
        cartList.forEach {
            total += it.price.value
        }
        return total
    }

    private fun setTotalPriceTextView(price: Double){
        binding.totalTextCard.text = String.format(getString(R.string.total_cart_price), price.noDecimalIfZeroString())
    }

    private fun treatFailure(failure: Failure) {
        binding.fragmentLoader.visibility = View.GONE

        when(failure) {
            is Failure.GetProductsFailure -> {
                binding.centerCardView.visibility = View.VISIBLE
                binding.emptyCartTextView.visibility = View.GONE
                binding.failureCartTextView.visibility = View.VISIBLE
            }
            is Failure.DeleteProductFromCartFailure -> {
                Toast.makeText(
                    context,
                    context?.let { failure.product.name.formatCapitalized(it, R.string.remove_product_cart_failure) },
                    Toast.LENGTH_SHORT
                ).show()
            }
            is Failure.EmptyCartFailure -> {
                Toast.makeText(
                    context,
                    getString(R.string.empty_cart_failure),
                    Toast.LENGTH_SHORT
                ).show()
            }
            else -> Log.e(TAG_UNTREATED_FAILURE, failure.toString())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}