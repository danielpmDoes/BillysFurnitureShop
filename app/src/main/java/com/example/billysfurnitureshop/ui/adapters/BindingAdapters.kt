package com.example.billysfurnitureshop.ui.adapters

import android.util.Log
import android.widget.ImageView
import androidx.core.net.toUri
import coil.load
import java.lang.Exception

fun bindImage(imgView: ImageView, imgUrl: String?) : Boolean {
    try {
        imgUrl?.let {
            val imgUri = imgUrl.toUri().buildUpon().scheme("https").build()
            imgView.load(imgUri)
            return true
        }
        return false
    } catch (e: Exception) {
        Log.e("bindImage", e.message?: "No message")
        return false
    }
}