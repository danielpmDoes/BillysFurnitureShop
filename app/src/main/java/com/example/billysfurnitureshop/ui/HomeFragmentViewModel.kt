package com.example.billysfurnitureshop.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.billysfurnitureshop.core.BaseViewModel
import com.example.billysfurnitureshop.data.models.Product
import com.example.billysfurnitureshop.uc.AddProductToCartUC
import com.example.billysfurnitureshop.uc.GetProductsUC
import com.example.billysfurnitureshop.utils.Constants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeFragmentViewModel @Inject constructor(
    private val getProductsUC: GetProductsUC,
    private val addProductToCartUC: AddProductToCartUC
) : BaseViewModel() {

    private val _homeProductList = MutableLiveData<List<Product>>()
    val homeProductList : LiveData<List<Product>>
        get() = _homeProductList

    private val _productAdded = MutableSharedFlow<Product>()
    val productAdded : SharedFlow<Product>
        get() = _productAdded

    fun getHomeProductList() {
        getProductsUC.execute(
            { it.either(::handleFailure, ::handleProductList) },
            GetProductsUC.Params(Constants.TYPE_HOME_LIST),
            viewModelScope
        )
    }

    private fun handleProductList(list: List<Product>) {
        _homeProductList.postValue(list)
    }

    fun addToCart(productToAdd: Product) {
        addProductToCartUC.execute(
            { it.either(::handleFailure, ::handleAddedToCart) },
            AddProductToCartUC.Params(productToAdd),
            viewModelScope
        )
    }

    private fun handleAddedToCart(product: Product) {
        viewModelScope.launch {
            _productAdded.emit(product)
        }
    }

}