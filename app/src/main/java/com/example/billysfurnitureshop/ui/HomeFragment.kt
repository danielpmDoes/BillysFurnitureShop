package com.example.billysfurnitureshop.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.example.billysfurnitureshop.R
import com.example.billysfurnitureshop.core.Failure
import com.example.billysfurnitureshop.core.formatCapitalized
import com.example.billysfurnitureshop.data.models.Product
import com.example.billysfurnitureshop.databinding.FragmentHomeBinding
import com.example.billysfurnitureshop.ui.adapters.ProductAdapter
import com.example.billysfurnitureshop.utils.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class HomeFragment : Fragment() {

    companion object {
        const val ARG_TAG = "homeFragment"
    }

    private val mViewModel : HomeFragmentViewModel by viewModels()

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var productAdapter : ProductAdapter
    private lateinit var toast : Toast

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        toast = Toast(context)
        context?.let { productAdapter = ProductAdapter(it, ARG_TAG, mutableListOf(), onAddToCart = ::onAddToCart) }

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        context?.let {
            binding.homeRecyclerView.layoutManager = GridLayoutManager(it, 2)
            binding.homeRecyclerView.adapter = productAdapter
        }

        initiateUI()
        initiateObservers()

        super.onViewCreated(view, savedInstanceState)
    }

    private fun initiateUI() {
        binding.fragmentLoader.visibility = View.VISIBLE
        mViewModel.getHomeProductList()
        binding.homeRecyclerView.adapter = productAdapter
    }

    private fun initiateObservers() {
        mViewModel.failure.observe(viewLifecycleOwner, ::treatFailure)

        mViewModel.homeProductList.observe(viewLifecycleOwner) { list ->
            binding.homeRecyclerView.visibility = View.VISIBLE
            binding.fragmentLoader.visibility = View.GONE
            productAdapter.updateList(list)
            binding.failureCardView.visibility = View.GONE
        }

        lifecycleScope.launchWhenStarted {
            mViewModel.productAdded.collectLatest { product ->
                context?.let {
                    toast.cancel()
                    toast = Toast.makeText(
                        context,
                        product.name.formatCapitalized(it ,R.string.product_added),
                        Toast.LENGTH_SHORT
                    )
                    toast.show()
                }

            }
        }
    }

    private fun onAddToCart(product : Product) {
        mViewModel.addToCart(product)
    }

    private fun treatFailure(failure: Failure) {
        binding.fragmentLoader.visibility = View.GONE

        when(failure) {
            is Failure.GetProductsFailure -> {
                showFailureCard()
            }
            is Failure.ServerErrorCode -> {
                showFailureCard()
                Log.e("Server Error Code", failure.errorCode.toString())
            }
            is Failure.ServerException -> {
                showFailureCard()
                Log.e("Server Exception", failure.exceptionMessage)
            }
            is Failure.AddProductToCartFailure -> {
                Toast.makeText(
                    context,
                    context?.let { failure.product.name.formatCapitalized(it, R.string.add_product_cart_failure) },
                    Toast.LENGTH_SHORT
                ).show()
            }
            else -> Log.e(Constants.TAG_UNTREATED_FAILURE, failure.toString())
        }
    }

    private fun showFailureCard() {
        binding.failureCardView.visibility = View.VISIBLE
        binding.homeRecyclerView.visibility = View.INVISIBLE
    }
}