package com.example.billysfurnitureshop.uc

import com.example.billysfurnitureshop.core.Either
import com.example.billysfurnitureshop.core.Failure
import com.example.billysfurnitureshop.core.UseCase
import com.example.billysfurnitureshop.data.models.Product
import com.example.billysfurnitureshop.data.repository.ProductRepository
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject

class RemoveProductFromCartUC @Inject constructor(
    val productRepository: ProductRepository
) : UseCase<RemoveProductFromCartUC.Params, Product, CoroutineScope>() {

    class Params(val product: Product)

    override suspend fun run(params: Params): Either<Failure, Product> {
        return when(val res = productRepository.removeProductFromCart(params.product)) {
            is Either.Right -> Either.Right(params.product)
            is Either.Left -> {
                res.a.logPossibleRoomException()
                Either.Left(Failure.DeleteProductFromCartFailure(params.product))
            }
        }
    }

}