package com.example.billysfurnitureshop.uc

import com.example.billysfurnitureshop.core.Either
import com.example.billysfurnitureshop.core.Failure
import com.example.billysfurnitureshop.core.UseCase
import com.example.billysfurnitureshop.data.models.Product
import com.example.billysfurnitureshop.data.repository.ProductRepository
import com.example.billysfurnitureshop.utils.Constants
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject

class GetProductsUC @Inject constructor(
    private val productRepository: ProductRepository
): UseCase<GetProductsUC.Params, List<Product>, CoroutineScope>() {

    class Params(val listType: String)

    override suspend fun run(params: Params): Either<Failure, List<Product>> {

        return when(val res = productRepository.getProducts(params.listType)) {
            is Either.Right -> {
                if(res.b.isEmpty() && params.listType == Constants.TYPE_HOME_LIST)
                    Either.Left(Failure.GetProductsFailure)
                else
                    Either.Right(res.b)
            }
            is Either.Left ->{
                res.a.logPossibleRoomException()
                Either.Left(Failure.GetProductsFailure)
            }
        }
    }

}