package com.example.billysfurnitureshop.uc

import com.example.billysfurnitureshop.core.Either
import com.example.billysfurnitureshop.core.Failure
import com.example.billysfurnitureshop.core.UseCase
import com.example.billysfurnitureshop.data.repository.ProductRepository
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject

class EmptyCartUC @Inject constructor(
    val productRepository: ProductRepository
) : UseCase<Unit, Unit, CoroutineScope>() {
    override suspend fun run(params: Unit): Either<Failure, Unit> {

        return when(val res = productRepository.emptyCart()) {
            is Either.Right -> Either.Right(Unit)
            is Either.Left -> {
                res.a.logPossibleRoomException()
                Either.Left(Failure.EmptyCartFailure)
            }
        }

    }

}