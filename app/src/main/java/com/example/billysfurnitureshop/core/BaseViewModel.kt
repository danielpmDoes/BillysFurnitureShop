package com.example.billysfurnitureshop.core

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel() {

    private var _failure = MutableLiveData<Failure>()
    val failure: LiveData<Failure>
        get() = _failure

    fun handleFailure(failure: Failure) {
        this._failure.postValue(failure)
    }

}