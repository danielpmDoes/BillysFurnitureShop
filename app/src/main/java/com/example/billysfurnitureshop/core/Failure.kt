package com.example.billysfurnitureshop.core

import android.util.Log
import com.example.billysfurnitureshop.data.models.Product
import com.example.billysfurnitureshop.utils.Constants

sealed class Failure {
    class ServerErrorCode(val errorCode: Int): Failure()
    class ServerException(val exceptionMessage: String): Failure()

    object GetProductsFailure : Failure()
    object EmptyCartFailure : Failure()
    class RoomExceptionFailure(val message: String) : Failure()
    class DeleteProductFromCartFailure(val product: Product) : Failure()
    class AddProductToCartFailure(val product: Product) : Failure()

    fun logPossibleRoomException() {
        if(this is RoomExceptionFailure)
            Log.e(Constants.TAG_ROOM_EXCEPTION, this.message)
    }
}
