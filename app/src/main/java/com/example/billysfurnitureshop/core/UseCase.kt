package com.example.billysfurnitureshop.core

import kotlinx.coroutines.*

abstract class UseCase<in Params, out Type, in Scope> where Type : Any, Scope : CoroutineScope {

    abstract suspend fun run(params: Params): Either<Failure, Type>

    fun execute(onResult: (Either<Failure, Type>) -> Unit, params: Params, scope: Scope) {

        scope.launch {
            val deferred = scope.async { run(params) }

            withContext(Dispatchers.IO) {
                onResult.invoke(deferred.await())
            }
        }
    }

    class None
}