package com.example.billysfurnitureshop.core

import android.content.Context
import java.util.*
import kotlin.math.absoluteValue

fun String.capitalize() : String {
    return replaceFirstChar {
        if (it.isLowerCase()) it.titlecase(Locale.ROOT)
        else it.toString()
    }
}

fun String.formatCapitalized(context: Context, resId: Int) : String{
    return String.format(
        context.getString(resId),
        replaceFirstChar {
            if (it.isLowerCase()) it.titlecase(Locale.ROOT)
            else it.toString()
        }
    )
}

fun Double.noDecimalIfZeroString() : String {
    //If the decimal value is .0, we don't show it on screen
    return if (this.absoluteValue % 1.0 < 0.0005 )
        String.format("%.0f", this)
    else
        this.toString()
}