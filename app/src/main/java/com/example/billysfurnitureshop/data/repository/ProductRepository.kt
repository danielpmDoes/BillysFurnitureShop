package com.example.billysfurnitureshop.data.repository

import com.example.billysfurnitureshop.core.Either
import com.example.billysfurnitureshop.core.Failure
import com.example.billysfurnitureshop.data.models.Product

interface ProductRepository {

    suspend fun getProducts(listType: String) : Either<Failure, List<Product>>
    suspend fun addProductToCart(product: Product) : Either<Failure, Unit>
    suspend fun removeProductFromCart(product: Product) : Either<Failure, Unit>
    suspend fun emptyCart() : Either<Failure, Unit>

}