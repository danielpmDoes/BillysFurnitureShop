package com.example.billysfurnitureshop.data.room

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [ProductRoomEntity::class], version = 1, exportSchema = false)
abstract class ProductRoomDatabase : RoomDatabase() {

    abstract fun productDao(): ProductRoomDao

}