package com.example.billysfurnitureshop.data.api

import com.example.billysfurnitureshop.data.models.EntityProducts
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("api/bins/9p08")
    suspend fun getProducts(): Response<EntityProducts>

}