package com.example.billysfurnitureshop.data.room

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.billysfurnitureshop.data.models.Info
import com.example.billysfurnitureshop.data.models.Price
import com.example.billysfurnitureshop.data.models.Product
import com.example.billysfurnitureshop.utils.Constants
import com.google.gson.Gson

@Entity(tableName = Constants.TABLE_CART)
data class ProductRoomEntity(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val name: String,
    val price: String,
    val info: String,
    val type: String,
    val imgUrl: String
) {
    fun toDomainModel() = Product(
        id,
        name,
        Gson().fromJson(price, Price::class.java),
        Gson().fromJson(info, Info::class.java),
        type,
        imgUrl
    )
}