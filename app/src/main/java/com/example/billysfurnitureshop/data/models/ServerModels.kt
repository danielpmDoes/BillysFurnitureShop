package com.example.billysfurnitureshop.data.models

data class EntityProducts(
    val products : List<EntityProduct>
)

data class EntityProduct(
    val id: String,
    val name: String,
    val price: EntityPrice,
    val info: EntityInfo,
    val type: String,
    val imageUrl: String
) {
    fun toDomain(): Product = Product(id.toInt(), name, price.toDomain(), info.toDomain(), type, imageUrl)
}

data class EntityPrice(
    val value: Double,
    val currency: String
) {
    fun toDomain(): Price = Price(value, currency)
}

data class EntityInfo(
    val material: String? = "",
    val numberOfSeats: Int? = 0,
    val color: String
) {
    fun toDomain(): Info = Info(material, numberOfSeats, color)
}