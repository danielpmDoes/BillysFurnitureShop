package com.example.billysfurnitureshop.data.room

import androidx.room.*
import com.example.billysfurnitureshop.utils.Constants

@Dao
interface ProductRoomDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertProduct(item: ProductRoomEntity)

    @Update
    suspend fun updateProduct(item: ProductRoomEntity)

    @Delete
    suspend fun removeProduct(item: ProductRoomEntity)

    @Query("SELECT * from ${Constants.TABLE_CART} ORDER BY id ASC")
    suspend fun getCartTable() : Array<ProductRoomEntity>

    @Query("SELECT COUNT(id) FROM ${Constants.TABLE_CART}")
    suspend fun getNumOfProducts() : Int

    @Query("DELETE FROM ${Constants.TABLE_CART}")
    suspend fun deleteTable()
}