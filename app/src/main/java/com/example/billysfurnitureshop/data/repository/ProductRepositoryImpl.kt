package com.example.billysfurnitureshop.data.repository

import com.example.billysfurnitureshop.core.Either
import com.example.billysfurnitureshop.core.Failure
import com.example.billysfurnitureshop.data.api.ApiService
import com.example.billysfurnitureshop.data.models.Product
import com.example.billysfurnitureshop.data.room.ProductRoomDao
import com.example.billysfurnitureshop.utils.Constants
import java.lang.Exception
import javax.inject.Inject

class ProductRepositoryImpl @Inject constructor(
    private val productRoomDao: ProductRoomDao,
    private val apiService: ApiService
) : ProductRepository {

    override suspend fun getProducts(listType: String): Either<Failure, List<Product>> {
        return when(listType) {
            Constants.TYPE_HOME_LIST -> getServerProductList()
            else -> getLocalCartList()
        }
    }

    private suspend fun getServerProductList() : Either<Failure, List<Product>> {
        return try {
            val response = apiService.getProducts()
            when (response.isSuccessful) {
                true -> {
                    val parsedEntityProductList = response.body()?.products ?: listOf()
                    Either.Right(parsedEntityProductList.map { it.toDomain() })
                }
                false -> {
                    Either.Left(Failure.ServerErrorCode(response.code()))
                }
            }
        } catch (exception: Throwable) {
            Either.Left(Failure.ServerException(exception.message?: "No message"))
        }
    }

    private suspend fun getLocalCartList() : Either<Failure, List<Product>> {
        return try {
            Either.Right(productRoomDao.getCartTable().map { it.toDomainModel() })
        } catch (e: Exception) {
            Either.Left(Failure.RoomExceptionFailure(e.message?: "No message"))
        }
    }

    override suspend fun addProductToCart(product: Product) : Either<Failure, Unit> {
        return try {
            productRoomDao.insertProduct(product.toProductDao())
            Either.Right(Unit)
        } catch (e: Exception) {
            Either.Left(Failure.RoomExceptionFailure(e.message?: "No message"))
        }
    }

    override suspend fun removeProductFromCart(product: Product): Either<Failure, Unit> {
        return try {
            productRoomDao.removeProduct(product.toProductDao())
            Either.Right(Unit)
        } catch (e: Exception) {
            Either.Left(Failure.RoomExceptionFailure(e.message?: "No message"))
        }
    }

    override suspend fun emptyCart(): Either<Failure, Unit> {
        return try {
            productRoomDao.deleteTable()
            Either.Right(Unit)
        } catch (e: Exception) {
            Either.Left(Failure.RoomExceptionFailure(e.message?: "No message"))
        }
    }


}