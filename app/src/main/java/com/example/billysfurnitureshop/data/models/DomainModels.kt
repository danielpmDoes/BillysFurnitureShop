package com.example.billysfurnitureshop.data.models

import com.example.billysfurnitureshop.data.room.ProductRoomEntity
import com.google.gson.Gson

//Could be sealed class ProductType
const val TYPE_COUCH = "couch"
const val TYPE_CHAIR = "chair"

data class Product(
    val id: Int,
    val name: String,
    val price: Price,
    val info: Info,
    val type: String,
    val imgUrl: String
) {
    fun toProductDao() = ProductRoomEntity(id, name, Gson().toJson(price), Gson().toJson(info), type, imgUrl)
}

data class Price(
    val value: Double,
    val currency: String
)

data class Info(
    val material: String? = "",
    val numberOfSeats: Int? = 0,
    val color: String
)