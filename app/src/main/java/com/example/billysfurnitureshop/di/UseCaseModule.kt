package com.example.billysfurnitureshop.di

import com.example.billysfurnitureshop.data.repository.ProductRepository
import com.example.billysfurnitureshop.uc.AddProductToCartUC
import com.example.billysfurnitureshop.uc.RemoveProductFromCartUC
import com.example.billysfurnitureshop.uc.EmptyCartUC
import com.example.billysfurnitureshop.uc.GetProductsUC
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object UseCaseModule {

    @Provides
    @Singleton
    fun providesGetProductsUC(repository: ProductRepository) : GetProductsUC {
        return GetProductsUC(repository)
    }

    @Provides
    @Singleton
    fun providesAddProductToCartUC(repository: ProductRepository) : AddProductToCartUC {
        return AddProductToCartUC(repository)
    }

    @Provides
    @Singleton
    fun providesDeleteProductFromCartUC(repository: ProductRepository) : RemoveProductFromCartUC {
        return RemoveProductFromCartUC(repository)
    }

    @Provides
    @Singleton
    fun providesEmptyCartUC(repository: ProductRepository) : EmptyCartUC {
        return EmptyCartUC(repository)
    }

}