package com.example.billysfurnitureshop.di

import com.example.billysfurnitureshop.data.api.ApiService
import com.example.billysfurnitureshop.data.repository.ProductRepository
import com.example.billysfurnitureshop.data.repository.ProductRepositoryImpl
import com.example.billysfurnitureshop.data.room.ProductRoomDao
import com.example.billysfurnitureshop.data.room.ProductRoomDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object AppModule {

    @Provides
    @Singleton
    fun provideProductDao(roomDatabase: ProductRoomDatabase) : ProductRoomDao {
        return roomDatabase.productDao()
    }

    @Provides
    @Singleton
    fun provideProductRepository(productRoomDao: ProductRoomDao, apiService: ApiService): ProductRepository {
        return ProductRepositoryImpl(productRoomDao, apiService)
    }

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)
}